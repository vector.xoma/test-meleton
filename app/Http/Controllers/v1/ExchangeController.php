<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Services\ConvertCurrencyService;
use App\Services\RateService;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class ExchangeController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function rates(Request $request): JsonResponse
    {
        return response()->json(RateService::getRate($request->get('filter') ?? []));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function convert(Request $request): JsonResponse
    {
        return response()->json(ConvertCurrencyService::convert($request->all() ?? []));
    }

}
