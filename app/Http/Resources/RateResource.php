<?php

namespace App\Http\Resources;

use App\Services\CalculationCommissionService;
use Illuminate\Http\Resources\Json\JsonResource;

class RateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (isset($this->currency)){
            return [
                'currency' => $this->currency,
                '15m' => (new CalculationCommissionService())->calculateAmountIncludingCommission($this->fifteen_minutes),
                'last' => (new CalculationCommissionService())->calculateAmountIncludingCommission($this->last),
                'buy' => (new CalculationCommissionService())->calculateAmountIncludingCommission($this->buy),
                'sell' => (new CalculationCommissionService())->calculateAmountIncludingCommission($this->sell),
                'symbol' => $this->symbol,
            ];
        }
        else{
            return [];
        }

    }
}
