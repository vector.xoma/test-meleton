<?php
namespace App\Services;

use App\Currencies;
use App\Http\Resources\RateResource;
use App\Http\Resources\RateResourceCollection;
use Spatie\QueryBuilder\QueryBuilder;

class RateService
{
    /**
     * @param array $filterData
     * @return RateResource|RateResourceCollection
     */
    public static function getRate(array $filterData = [])
    {
        CurrencyLoaderService::getRatesDataFromService();
        CurrencyLoaderService::setRatesDataToDataBase();

        if ($filterData){
            return self::getOneCurrencyRate();
        }
        else{
            return self::getAllCurrencyRate();
        }
    }

    /**
     * @return RateResource
     */
    public static function getOneCurrencyRate(): RateResource
    {

        $oneCurrencyRate = QueryBuilder::for(Currencies::class)
            ->allowedFilters('currency')
            ->first();
        if (!is_null($oneCurrencyRate)){
            return new RateResource($oneCurrencyRate);
        }
        return new RateResource(null);
    }

    /**
     * @return RateResourceCollection
     */
    private static function getAllCurrencyRate(): RateResourceCollection
    {
        $allCurrencyRate = QueryBuilder::for(Currencies::class)
            ->allowedFilters('currency')
            ->allowedSorts('buy')
            ->get();
        return new RateResourceCollection($allCurrencyRate);
    }

}
