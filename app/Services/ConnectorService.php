<?php
namespace App\Services;

use Illuminate\Support\Facades\Http;
use \Illuminate\Http\Client\Response;

class ConnectorService
{

    /** @var string  */
    private $host = "";

    /**
     * @param string $host
     */
    public function __construct(string $host)
    {
        $this->host = $host;
    }

    /**
     * @param string $url
     * @return Response
     */
    public function get(string $url): Response
    {
        return Http::get($this->host . $url);
    }

}
