<?php
namespace App\Services;



use App\Currencies;
use Illuminate\Http\Client\Request;

class ConvertCurrencyService
{
    /** @var Currencies */
    private static $currencyFrom;

    /** @var Currencies */
    private static $currencyTo;

    /** @var float  */
    private static $course;

    /**
     * @param array $data
     * @return array
     */
    public static function convert(array $data = []): array
    {
        $result = [];
        if ($data){
            self::$currencyFrom = Currencies::where('currency', $data['currency_from'])->first();
            self::$currencyTo = Currencies::where('currency', $data['currency_to'])->first();

            self::$course = self::getCurrencyCourse();

            $rate = self::getCurrencyRate(floatval($data['value']));

            $result = [
                "currency_from" => $data['currency_from'],
                "currency_to" => $data['currency_to'],
                "value" => $data['value'],
                "converted_value" => 1.0,
                "rate" => $rate,
                "created_at" => time()
            ];
        }

        return $result;
    }

    /**
     * @return float
     */
    private static function getCurrencyCourse(): float
    {
        $baseCurrency = (new CalculationCommissionService())
            ->calculateAmountIncludingCommission(self::$currencyFrom->buy);

        $conversionCurrency  = (new CalculationCommissionService())
            ->calculateAmountIncludingCommission(self::$currencyTo->sell);

        $course = $conversionCurrency * 1 / $baseCurrency;

        return round($course, 2);
    }

    /**
     * @param float $value
     * @return float
     */
    private static function getCurrencyRate(float $value): float
    {
        return round(self::$course * $value / 1, 10);
    }
}
