<?php
namespace App\Services;


class CalculationCommissionService
{
    private $commissionPercentage ;

    public function __construct()
    {
        $this->commissionPercentage = env('COMMISSION_PERCENTAGE');
    }

    /**
     * @param float $rate
     * @return float
     */
    public function calculateAmountIncludingCommission (float $rate): float
    {
        $rate = $rate + ($rate * ($this->commissionPercentage / 100));
        $rate = round($rate, 10);
        return $rate;
    }

}
