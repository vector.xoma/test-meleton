<?php


namespace App\Services;


use App\Currencies;

class CurrencyLoaderService
{
    /** @var  array */
    private static $data;


    public static function setRatesDataToDataBase()
    {
        Currencies::truncate();

        foreach (self::$data AS $key => $rates){
            $currencyModel = new Currencies();
            $currencyModel->currency = $key;
            $currencyModel->fifteen_minutes = $rates['15m'];
            $currencyModel->last = $rates['last'];
            $currencyModel->buy = $rates['buy'];
            $currencyModel->sell = $rates['sell'];
            $currencyModel->symbol = $rates['symbol'];
            $currencyModel->updated = time();
            $currencyModel->save();
        }
    }

    public static function getRatesDataFromService()
    {
        self::$data = (new ConnectorService(env("BTC_URL")))->get('ticker')->json();
    }

    public static function checkLastUpdatedRate()
    {
        // --
    }

}

