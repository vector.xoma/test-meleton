Общее время затраченное на оба задания: 9 часов

первую часть выполнил приблизительно за 1 час

вторую часть пилил все остальное время т.е. 8 часов

-------------

#### Задание 1

``` sql
SELECT
    u.id, CONCAT(u.first_name, " ", u.last_name) AS name, b.author, group_concat(b.name SEPARATOR ',') AS books, COUNT(b.name) AS cnt
FROM users AS u
    INNER JOIN user_books AS ub ON ub.user_id = u.id
    INNER JOIN books AS b ON ub.book_id = b.id
WHERE u.age BETWEEN 7 AND 17
GROUP BY b.author
HAVING COUNT(b.name) = 2
```

[Структура таблиц тут](https://gitlab.com/vector.xoma/test-meleton/-/raw/master/test-meleton-1)

#### Задание 2

1. composer update
2. Добавить в .env файл след. параметры:
    - BTC_URL=https://blockchain.info/ 
    - COMMISSION_PERCENTAGE=2
    - Указать настройки для доступа к БД
    
3. В папке проекта выполнить
   
   `php artisan jwt:secret`

4. Выполнить миграци
   
    `php artisan migrate:install`
   
    `php artisan migrate`

5. В папке проекта выполнить

    `php artisan db:seed --class=UserSeder`

P.S. в проекте используется полноценная авторизация на токенах
```
POST http://test-meleton.loc/api/v1/login
Content-Type: application/json
Accept: application/json

{
  "email": "user@mail.com",
  "password": "123456"
}
```
