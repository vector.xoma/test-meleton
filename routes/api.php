<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'v1'], function () {
    Route::post('login', 'v1\AuthController@login')->name('login');
    Route::post('logout', 'v1\AuthController@logout')->name('logout');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('rates', 'v1\ExchangeController@rates')->name('exchange.rates');
        Route::post('convert', 'v1\ExchangeController@convert')->name('exchange.convert');
    });
});

//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
