<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->id();
            $table->string('currency')->nullable(false)->index('idx_currency_name');
            $table->string('fifteen_minutes')->nullable(false);
            $table->string('last')->nullable(false);
            $table->string('buy')->nullable(false);
            $table->string('sell')->nullable(false);
            $table->string('symbol')->nullable(false);
            $table->integer('updated')->nullable(false)->default(0)->index('idx_currency_updated');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}

